package SavingsAccountClass;

public class SavingsAccount {
    public static float annualInterestRate;
    private double savingsBalance;

    SavingsAccount(double savingsBalance, float annualInterestRate) {
        this.savingsBalance = savingsBalance;
        this.annualInterestRate = annualInterestRate;
        this.savingsBalance = savingsBalance+calculateMonthlyInterest();
    }

    public double getSavingsBalance() {
        return savingsBalance;
    }

    public void setSavingsBalance(double savingsBalance) {
        this.savingsBalance = savingsBalance + calculateMonthlyInterest();
    }

    public double calculateMonthlyInterest() {
        double total=(annualInterestRate * savingsBalance) / 12.0;
        return total;
    }

    @Override
    public String toString() {
        return "Next's month interest: " + calculateMonthlyInterest() + "\nNew Balance is: " + getSavingsBalance();
    }
}
