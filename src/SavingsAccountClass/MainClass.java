package SavingsAccountClass;

import java.util.Scanner;

public class MainClass {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        double savingBalance = scanner.nextDouble();
        float annualInterestRate = scanner.nextInt() / 100.0f;

        SavingsAccount saver = new SavingsAccount(savingBalance, annualInterestRate);
        System.out.println(saver.toString());
    }

}
