package PayableSystem;

import PayableSystem.Employee;

public class CommissionEmployee extends Employee {
    private double grossSales;
    private double commissionRate;


    CommissionEmployee(String firstName, String lastName, String socialSecurityNumber, double grossSales, double commissionRate, int day, int mont, int year) {

        super(firstName, lastName, socialSecurityNumber, day, mont, year);
        if (grossSales < 0.0)
            throw new IllegalArgumentException("Gross sales must be >= 0.0");

        if (commissionRate <= 0.0 || commissionRate >= 1.0)
            throw new IllegalArgumentException("Commission rate must be > 0.0 and < 1.0");

        this.grossSales = grossSales;
        this.commissionRate = commissionRate;
    }

    public double getGrossSales() {
        return grossSales;
    }

    public void setGrossSales(double grossSales) {
        if (grossSales < 0.0)
            throw new IllegalArgumentException("Gross sales must be >= 0.0");
        this.grossSales = grossSales;
    }

    public double getCommissionRate() {
        return commissionRate;
    }

    public void setCommissionRate(double commissionRate) {
        if (commissionRate <= 0.0 || commissionRate >= 1.0)
            throw new IllegalArgumentException("Commission rate must be > 0.0 and < 1.0");
        this.commissionRate = commissionRate;
    }

    public double getPaymentAmount() {
        return commissionRate * grossSales;
    }

    @Override
    public String toString() {
        return String.format("%n Commission employee: %n%s%n%s: %.2f%n%s: %.2f%n", super.toString(),
                "gross sales", grossSales,
                "commission rate", commissionRate);
    }
}
