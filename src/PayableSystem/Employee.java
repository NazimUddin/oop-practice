package PayableSystem;

public abstract class Employee extends Date implements Payable {
    private final String firstName;
    private final String lastName;
    private final String socialSecurityNumber;
    private String birthDate;

    Employee(String firstName, String lastName, String socialSecurityNumber, int day, int month, int year) {
        super(day, month, year);
        this.firstName = firstName;
        this.lastName = lastName;
        this.socialSecurityNumber = socialSecurityNumber;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getSocialSecurityNumber() {
        return socialSecurityNumber;
    }

    public abstract double getPaymentAmount();
    @Override
    public String toString() {
        return String.format("%s: %s %s%n%s: %s",
                "commission employee", firstName, lastName,
                "social security number", socialSecurityNumber);
    }

}
