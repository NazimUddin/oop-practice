package PayableSystem;

import PayableSystem.Employee;

public class HourlyEmployee extends Employee {
    private double hour;
    private double wage;

    HourlyEmployee(String firstName, String lastName, String socialSecurityNumber, double wage, double hour, int day, int month, int year) {
        super(firstName, lastName, socialSecurityNumber, day, month, year);
        this.wage = wage;
        this.hour = hour;

    }

    public double getHour() {
        return hour;
    }

    public void setHour(double hour) {
        if (hour >= 0 && hour <= 168)
            throw new IllegalArgumentException("Hour must have in range 0 and 168");
        this.hour = hour;
    }

    public double getWage() {
        return wage;
    }

    public void setWage(double wage) {
        if (wage <= 0.0)
            throw new IllegalArgumentException("Wage must be greater than 0");
        this.wage = wage;
    }


    @Override
    public double getPaymentAmount()
    {
        if (getHour() <= 40)
            return getWage() * getHour();
        else
            return 40 * getWage() + (getHour() - 40) * getWage() * 1.5;
    }

    @Override
    public String toString()
    {
        return String.format("%n hourly employee: %n%s%n%s: $%,.2f; %s: %,.2f%n",
                super.toString(), "hourly wage", getWage(),
                "hours worked", getHour());
    }
}
