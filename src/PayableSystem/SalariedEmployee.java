package PayableSystem;

import java.time.LocalDate;

public class SalariedEmployee extends Employee {
    private double weeklySalary;

    SalariedEmployee(String firstName, String lastName, String socialSecurityNumber, double weeklySalary, int day, int month, int year) {
        super(firstName, lastName, socialSecurityNumber, day, month, year);
        if (weeklySalary < 0.0)
            throw new IllegalArgumentException("Weekly salary must be >= 0.0");

        this.weeklySalary = weeklySalary;
    }

    public double getWeeklySalary() {
        return weeklySalary;
    }

    public void setWeeklySalary(double weeklySalary) {
        if (weeklySalary < 0.0)
            throw new IllegalArgumentException("Weekly salary must be >= 0.0");

        this.weeklySalary = weeklySalary;
    }

    public int getCurrentMonth() {
        LocalDate today = LocalDate.now();
        int month = today.getMonthValue();
        return month;
    }

    @Override
    public double getPaymentAmount() {
        if (getMonth() == getCurrentMonth()) {
            return getWeeklySalary() + 100.00;
        } else {
            return getWeeklySalary();
        }
    }

    @Override
    public String toString() {
        return String.format("%n salaried employee: %n%s%n%s: $%,.2f%n",
                super.toString(), "weekly salary", getPaymentAmount());
    }

}
