package Java8Feature.InterfaceDefaultAndStaticMethod.MultipleInheritanceProblemSolve;

public class Test implements Left,Right {
    @Override
    public void M1() {
        //System.out.println("Implement from Test");
        Left.super.M1();
    }

    public static void main(String[] args) {
        Test t = new Test();
        t.M1();
    }
}
