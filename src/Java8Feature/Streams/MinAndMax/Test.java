package Java8Feature.Streams.MinAndMax;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.NoSuchElementException;

public class Test {
    Person alex = new Person("Alex", 23);
    Person john = new Person("John", 40);
    Person peter = new Person("Peter", 32);
    List<Person> people = Arrays.asList(alex, john, peter);

    // then
    Person minByAge = people
            .stream()
            .min(Comparator.comparing(Person::getAge))
            .orElseThrow(NoSuchElementException::new);

}
