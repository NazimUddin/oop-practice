package Java8Feature.Streams.MinAndMax;

import java.util.ArrayList;

public class minMaxPractice {
    public static void main(String[] args) {
        ArrayList<Integer> items = new ArrayList<>();
        items.add(20);
        items.add(20);
        items.add(25);
        items.add(22);
        Integer min = items.stream().min((i1,i2)->i1.compareTo(i2)).get();
        Integer max = items.stream().min((i1,i2)->-i1.compareTo(i2)).get();
        System.out.println(min);
    }
}
