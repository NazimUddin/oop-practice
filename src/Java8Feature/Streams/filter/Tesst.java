package Java8Feature.Streams.filter;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Tesst {
    public static void main(String[] args) {
        List<Integer> l1 = new ArrayList<>();

        l1.add(10);
        l1.add(0);
        l1.add(9);
        l1.add(8);
        List<Integer> l2 = l1.stream().filter(I->I%2==0).collect(Collectors.toList());
        System.out.println(l2);
        //
        List<Integer> l3 = new ArrayList<>();
//        for (int item: l1) {
//            l3.add(item*2);
//        }
        l3 = l1.stream().map(I->I*2).collect(Collectors.toList());
        System.out.println(l3);

    }
}
