package Java8Feature.LamdaExpression.ExampleOne.WithoutLamda.Example4SortingForCustomisedObject;

import java.util.Comparator;

public class Employee {
    private int no;
    private String name;

    public Employee(int no, String name) {
        this.no = no;
        this.name = name;
    }

    public int getNo() {
        return no;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return no+" = "+name;
    }
}
