package Java8Feature.LamdaExpression.ExampleOne.WithoutLamda.Example4SortingForCustomisedObject;

import java.util.Comparator;

public class MyCustomComparator implements Comparator<Employee> {
    @Override
    public int compare(Employee o1, Employee o2) {
        if (o1.getNo() > o2.getNo()) {
            return 1;
        } else if (o1.getNo() < o2.getNo()) {
            return -1;
        }
        return 0;
    }
}
