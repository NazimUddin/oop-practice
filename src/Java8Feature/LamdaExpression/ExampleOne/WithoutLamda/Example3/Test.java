package Java8Feature.LamdaExpression.ExampleOne.WithoutLamda.Example3;

public class Test {
    public static void main(String[] args) {
        MyRunnable runnable = new MyRunnable();
        Thread thread = new Thread(runnable);
        thread.start();

        for (int i = 0; i < 10; i++) {
            System.out.println("Run from main thread");
        }
    }
}
