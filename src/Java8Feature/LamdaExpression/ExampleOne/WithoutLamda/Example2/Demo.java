package Java8Feature.LamdaExpression.ExampleOne.WithoutLamda.Example2;

public class Demo implements Interf {
    @Override
    public void add(int a, int b) {
        System.out.println("Sum of a and b is "+ (a+b));
    }
}
