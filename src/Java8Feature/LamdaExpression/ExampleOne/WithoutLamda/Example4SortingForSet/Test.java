package Java8Feature.LamdaExpression.ExampleOne.WithoutLamda.Example4SortingForSet;

import java.util.ArrayList;
import java.util.TreeSet;

public class Test {
    public static void main(String[] args) {
        TreeSet<Integer> item = new TreeSet<>();
        item.add(20);
        item.add(0);
        item.add(10);
        item.add(25);
        item.add(30);
        System.out.println(item);
    }
}
