package Java8Feature.LamdaExpression.ExampleOne.WithoutLamda.Example4SortingForArray;

import java.util.ArrayList;
import java.util.Collections;

public class Test {
    public static void main(String[] args) {
        ArrayList<Integer> item = new ArrayList<>();
        item.add(20);
        item.add(0);
        item.add(10);
        item.add(25);
        item.add(30);
        System.out.println(item);

        Collections.sort(item, new MyComparator());
        System.out.println(item);
    }
}
