package Java8Feature.LamdaExpression.ExampleOne.WithLamda.Example4SortingForCustomisedObject;

import java.util.ArrayList;
import java.util.Collections;

public class Test {
    public static void main(String[] args) {
//        Employee employee = new Employee(20, "asif");
//        System.out.println(employee);

        ArrayList<Employee> employees = new ArrayList<>();
        employees.add(new Employee(10, "Asif"));
        employees.add(new Employee(30, "Nazim"));
        employees.add(new Employee(20, "Good"));
        System.out.println(employees);
        Collections.sort(employees, (o1,o2)-> (o1.getNo()>o2.getNo())?1:(o1.getNo()<o2.getNo())?1:0);
    }

}
