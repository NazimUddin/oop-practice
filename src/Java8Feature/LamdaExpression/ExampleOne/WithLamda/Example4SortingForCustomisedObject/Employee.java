package Java8Feature.LamdaExpression.ExampleOne.WithLamda.Example4SortingForCustomisedObject;

public class Employee {
    private int no;
    private String name;

    public int getNo() {
        return no;
    }

    public String getName() {
        return name;
    }

    public Employee(int no, String name) {
        this.no = no;
        this.name = name;
    }

    @Override
    public String toString() {
        return no+" = "+name;
    }
}
