package Java8Feature.LamdaExpression.ExampleOne.WithLamda.Example4SortingForArray;

import java.util.ArrayList;
import java.util.Collections;

public class Test {
    public static void main(String[] args) {
        ArrayList<Integer> item = new ArrayList<>();
        item.add(20);
        item.add(0);
        item.add(10);
        item.add(25);
        item.add(30);
        System.out.println(item);

//        Comparator<Integer> myComoparator = (Integer o1, Integer o2) -> {
//            if (o1 > o2) {
//                return -1;
//            } else if (o2 > o1)
//                return 1;
//            else return 0;
//        };

//        Collections.sort(item, myComoparator);

        Collections.sort(item, ( o1, o2) -> {
            if (o1 > o2) {
                return -1;
            } else if (o2 > o1)
                return 1;
            else return 0;
        });
        System.out.println(item);
    }

}
