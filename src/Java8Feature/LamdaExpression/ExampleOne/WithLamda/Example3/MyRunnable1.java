package Java8Feature.LamdaExpression.ExampleOne.WithLamda.Example3;

public interface MyRunnable1 {

    public static void main(String[] args) {

        Runnable r = ()-> {
            for (int i = 0; i < 10; i++) {
                System.out.println("Run from child");
            }
        };
        Thread thread = new Thread(r);
        thread.start();
        for (int i = 0; i < 10; i++) {
            System.out.println("Run from main thread");
        }

    }
}
