package Java8Feature.LamdaExpression.ExampleOne.WithLamda.Example2;

public class Test {
    public static void main(String[] args) {
        Interf i= (a,b)-> System.out.println("sum of a and b is "+(a+b));
        i.add(20,30);
        i.add(25,35);
    }
}
