package Java8Feature.LamdaExpression.ExampleOne.WithLamda.Example4SortingForSet;

import java.util.ArrayList;
import java.util.TreeSet;

public class Test {
    public static void main(String[] args) {
        TreeSet<Integer> item = new TreeSet<>((I1,I2)->(I1>I2)?-1:(I1<I2)?1:0);
        item.add(20);
        item.add(0);
        item.add(10);
        item.add(25);
        item.add(30);
        System.out.println(item);

    }
}
