package Java8Feature.LamdaExpression.ExampleOne.WithLamda.Example4SortingForMap;

import java.util.TreeMap;
import java.util.TreeSet;

public class Test {
    public static void main(String[] args) {
        //TreeMap<Integer, String> item = new TreeSet<>((I1, I2)->(I1>I2)?-1:(I1<I2)?1:0);
        TreeMap<Integer, String> item = new TreeMap<>();
        item.put(20,"haha");
        item.put(30,"hihi");
        item.put(10,"huhu");

        System.out.println(item);

    }
}
