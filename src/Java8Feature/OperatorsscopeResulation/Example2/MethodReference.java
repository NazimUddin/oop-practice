package Java8Feature.OperatorsscopeResulation.Example2;

public class MethodReference {
    public void M1(){
        for (int i = 0; i < 10; i++) {
            System.out.println("This is child thread");
        }
    }
    public static void main(String[] args) {
        MethodReference m = new MethodReference();
        Runnable r = m::M1;
        Thread t = new Thread(r);
        t.start();
        for (int i = 0; i < 10; i++) {
            System.out.println("This is parent");
        }

    }
}
