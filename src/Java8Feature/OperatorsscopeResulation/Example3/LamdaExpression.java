package Java8Feature.OperatorsscopeResulation.Example3;

public class LamdaExpression {
    public static void main(String[] args) {
        interf i = ()->{
          Sample s = new Sample();
          return s;
        };
        i.get();
    }
}
