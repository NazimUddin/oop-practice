package Java8Feature.BiFunctionalInterface.BiFunction.Example;

public class Employee {
    private String eno;
    private String name;
    private double dailyWage;

    public Employee(String eno, String name, double dailyWage) {
        this.eno = eno;
        this.name = name;
        this.dailyWage = dailyWage;
    }

    public String getEno() {
        return eno;
    }

    public String getName() {
        return name;
    }

    public double getDailyWage() {
        return dailyWage;
    }
}
