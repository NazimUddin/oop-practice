package Java8Feature.BiFunctionalInterface.BiFunction.Example;

import java.util.function.BiFunction;

public class Test {
    public static void main(String[] args) {
        Employee employee = new Employee("12","aa",120.0);
        TimeShit timeShit = new TimeShit("12",12);
        BiFunction<Employee,TimeShit, Double> f = (e,t)->e.getDailyWage()*t.getDays();

        System.out.println(f.apply(employee,timeShit));
    }
}
