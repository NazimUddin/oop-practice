package Java8Feature.BiFunctionalInterface.BiFunction.Example;

public class TimeShit {
    private String eno;
    private int days;

    public TimeShit(String eno, int days) {
        this.eno = eno;
        this.days = days;
    }

    public String getEno() {
        return eno;
    }

    public int getDays() {
        return days;
    }
}
