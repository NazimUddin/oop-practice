package Java8Feature.BiFunctionalInterface.BiConsumer;

import java.util.function.*;

public class Test {
    public static void main(String[] args) {
        BiConsumer<String,String> c = (s, s2) -> System.out.println(s+s2);

        c.accept("Asif ","Nazim");
    }
}
