package Java8Feature.PracticeOfConsumerPredicateAndFunction;


import java.util.ArrayList;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;

public class Test {
    public static void main(String[] args) {
        ArrayList<Student> students = new ArrayList<>();
        students = Populate(students);

        Predicate<Student> ckeckMarks = student -> student.getMarks() >= 60;

        Function<Student, String> checkGreads = student -> {
            int mark = student.getMarks();
            if (mark >= 80) {
                return "A+";
            } else if (mark >= 70) {
                return "A";
            } else if (mark >= 60) {
                return "B";
            } else
                return "F";

        };

        Consumer<Student> showStudent = student -> {
            System.out.println("Name " + student.getName());
            System.out.println("Marks " + student.getMarks());
            System.out.println("Grad " + checkGreads.apply(student));
        };

        for (Student student : students) {
            if (ckeckMarks.test(student)) {
                showStudent.accept(student);
            }
        }
    }

    public static ArrayList<Student> Populate(ArrayList<Student> students) {
        students.add(new Student("asif", 80));
        students.add(new Student("nazim", 80));
        students.add(new Student("nazim1", 60));
        students.add(new Student("nazim1", 50));
        return students;
    }
}
