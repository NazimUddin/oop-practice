package Java8Feature.Function.Example2CompareThenAndCompose;

import java.util.function.Function;

public class Test {
    public static void main(String[] args) {
        Function<String, String> f = s -> s.toUpperCase();
        Function<String, String> f1 = s -> s.substring(0,8);

        System.out.println(f.andThen(f1).apply("IIUC is the best university"));

        Function<Integer, Integer> ff = i->i+i;
        Function<Integer, Integer> ff1 = i->i*i*i;

        System.out.println(ff.andThen(ff1).apply(3));
        System.out.println(ff.compose(ff1).apply(3));


    }
}
