package Java8Feature.Function.UserAuthentication;

import java.util.ArrayList;
import java.util.function.Function;

public class Test {
    public static void main(String[] args) {
        Function<String, String> name = s -> s.toUpperCase();
        Function<String, String> nameUpdated = s -> s.substring(0,5);

        Function<String, String> pass = s -> s.toUpperCase();

        ArrayList<User> users = new ArrayList<>();

        users = Populate(users);
        for (User user: users) {
            if (name.andThen(nameUpdated).apply(user.getName()).equals("ASIF2") && pass.apply(user.getPass()).equals("123452")){
                System.out.println("Valid user is "+user.getName());
            }

        }

    }
    public static ArrayList<User> Populate(ArrayList<User> users){
        users.add(new User("Asif2","123452"));
        users.add(new User("Asif6","1234522"));
        users.add(new User("Asif3","1234523"));
        return users;
    }
}
