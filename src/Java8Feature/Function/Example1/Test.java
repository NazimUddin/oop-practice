package Java8Feature.Function.Example1;

import java.util.ArrayList;
import java.util.function.Function;
import java.util.function.Predicate;

public class Test {
    public static void main(String[] args) {
        ArrayList<Employee> employees = new ArrayList<>();
        employees = populate(employees);
       // System.out.println(employees);
        Function<ArrayList<Employee>,Long> f = employes -> {
            long totalAmount=0;
            for (Employee e: employes) {
                totalAmount += e.getSalary();
            }
            return totalAmount;

        };
        System.out.println("Total saraly is "+ f.apply(employees));

        Function<Employee,Long> f1 = employes -> {
            return employes.getSalary()+1000;

        };


        Predicate<Employee> p = e->e.getName().equals("Asif");

        for (Employee e: employees) {
            if(p.test(e)){
                System.out.println(e.getName()+"'s Incremented saraly is "+ f1.apply(e));
            }
        }

    }
    public static ArrayList<Employee> populate (ArrayList<Employee> employees){
        employees.add(new Employee("Asif","senior", 350000, "feni"));
        employees.add(new Employee("nazim","senior", 450000, "feni"));
        employees.add(new Employee("azam","CTO", 550000, "feni"));
        employees.add(new Employee("khairul","MD", 650000, "feni"));
        return employees;
    }

}
