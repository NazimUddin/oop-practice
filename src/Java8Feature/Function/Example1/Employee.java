package Java8Feature.Function.Example1;

public class Employee {
    private String name;
    private String designation;
    private long salary;
    private String city;

    public Employee(String name, String designation, long salary, String city) {
        this.name = name;
        this.designation = designation;
        this.salary = salary;
        this.city = city;
    }

    public String getName() {
        return name;
    }

    public String getDesignation() {
        return designation;
    }

    public long getSalary() {
        return salary;
    }

    public String getCity() {
        return city;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "name='" + name + '\'' +
                ", designation='" + designation + '\'' +
                ", salary=" + salary +
                ", city='" + city + '\'' +
                '}'+"\n";
    }
}
