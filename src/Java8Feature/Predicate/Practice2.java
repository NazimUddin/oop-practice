package Java8Feature.Predicate;

import java.util.function.Predicate;

public class Practice2 {
    public static void main(String[] args) {
        String [] names = {"Asif","Akib","Kalim","jamil","Khairul"};

        Predicate<String> p = name->name.startsWith("A");
        System.out.println("Print all name which is start with A");
        M1(p,names);

    }

    public static void M1(Predicate<String> p ,String[] names) {
        for (String name: names) {
            if(p.test(name)){
                System.out.println(name);
            }
        }

    }
}
