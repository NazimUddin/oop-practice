package Java8Feature.Predicate;

import java.util.function.Predicate;

public class Practice3 {
    public static void main(String[] args) {
        String [] names = {"","Akib","Kalim","","Khairul",null};

        //Predicate<String> p = name->name.isBlank(); // it only check ""
        Predicate<String> p = name->name!=null && name!="";
        System.out.println("Print all name which is not null");
        M1(p,names);

    }

    public static void M1(Predicate<String> p ,String[] names) {
        for (String name: names) {
            if(p.test(name)){
                System.out.println(name);
            }
        }

    }
}
