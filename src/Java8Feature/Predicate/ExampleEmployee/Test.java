package Java8Feature.Predicate.ExampleEmployee;

import java.util.ArrayList;
import java.util.function.Predicate;

public class Test {
    public static void main(String[] args) {
        ArrayList<Employee> employees = new ArrayList<>();
        employees = populate(employees);
        //System.out.println(employees);
        Predicate<Employee> p = employee -> employee.getDesignation().equals("CTO");
        findCTO(p, employees);

    }
    public static ArrayList<Employee> populate(ArrayList<Employee> employees){
        employees.add(new Employee("asif","senior", 350000, "feni"));
        employees.add(new Employee("nazim","senior", 450000, "feni"));
        employees.add(new Employee("azam","CTO", 550000, "feni"));
        employees.add(new Employee("khairul","MD", 650000, "feni"));
        return employees;
    }

    public static void findCTO(Predicate<Employee> p, ArrayList<Employee> employees) {
        for (Employee e: employees) {
            if (p.test(e)){
                System.out.println(e);
            }
        }
    }
}
