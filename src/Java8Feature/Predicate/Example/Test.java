package Java8Feature.Predicate.Example;

import java.util.ArrayList;
import java.util.function.Predicate;

public class Test {
    public static void main(String[] args) {
        ArrayList<User> persons = new ArrayList<>();
        Predicate<User> p = user -> user.getName().equals("Asif") && user.getPass().equals("1234");
        persons.add(new User("Asif", "1234"));
        persons.add(new User("Asif1", "1234"));
        persons.add(new User("Asif2", "1234"));
        m1(p, persons);

    }

    public static void m1(Predicate<User> p ,ArrayList<User> persons) {
        for (User user: persons) {
            if(p.test(user)){
                System.out.println("Valid User is "+user.getName());
            }
        }
    }
}
