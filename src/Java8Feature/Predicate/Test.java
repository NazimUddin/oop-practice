package Java8Feature.Predicate;

import java.util.function.Predicate;

public class Test {
    public static void main(String[] args) {
        int[] x = {0, 10, 20, 25};
        Predicate<Integer> p = I -> I > 10;
        Predicate<Integer> p1 = I -> I % 2 == 0;

        System.out.println("Number is greater then 10");
        M1(p,x);

        System.out.println("Number is even");
        M1(p1,x);

        System.out.println("Number is greater then 10 and even");
        M1(p.and(p1),x);

        System.out.println("Number is greater then 10 or even");
        M1(p.or(p1),x);

        System.out.println("Number is not greater then 10");
        M1(p.negate(),x);
    }

    public static void M1(Predicate<Integer> p, int[] x){
        for (int x1 : x) {
            if(p.test(x1)){
                System.out.println(x1);
            }
        }
    }
}
