package Java8Feature.Supplier;

import java.util.Date;
import java.util.function.Supplier;

public class Test {
    public static void main(String[] args) {
        //Example 1
        Supplier<Date> currentDate = () -> new Date();

        System.out.println("Current date is " + currentDate.get());

        //Example 2
        Supplier<String> name = () -> {
            String[] names = {"january", "Feb", "Mar", "April", "May"};
            int random = (int) (Math.random() * 4);
            return names[random];
        };
        System.out.println("Name is " + name.get());

        //Example 3 sending OTP
        Supplier<String> OTP = () -> {
            String otp = "";
            for (int i = 0; i < 6; i++) {
                otp += (int) (Math.random() * 10);
            }
            return otp;
        };

        System.out.println("Your OTP is " + OTP.get());
    }
}
