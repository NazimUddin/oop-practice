package Inharitance;

import java.util.Scanner;

public class Application {
    public static void main(String[] args) {
        Scanner scanner= new Scanner(System.in);
        byte no= scanner.nextByte();
        Employee employee=getEmployee(no);
        System.out.printf("%n%s:%n%n%s%n", "Updated employee information obtained by toString", employee.toString());

    }
    public static Employee getEmployee(byte no){
        switch (no){
            case 1:
                return new CommissionEmployee(
                        "Asif", "Nazim", "222-22-2222", 10000, .06);
            case 2:
                return  new BasePlusCommissionEmployee(
                    "Sakif", "Uddin", "333-33-3333", 5000, .04, 300);
            case 3:
                return  new HourlyEmployee("Jahid","Uddin","444-44-444",2000,20);
        }
        return null;
    }
}
