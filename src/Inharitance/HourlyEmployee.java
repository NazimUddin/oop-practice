package Inharitance;

public class HourlyEmployee extends Employee {
    private double hour;
    private double wage;

    HourlyEmployee(String firstName, String lastName, String socialSecurityNumber, double wage, double hour) {
        super(firstName, lastName, socialSecurityNumber);
        this.wage = wage;
        this.hour = hour;

    }

    public double getHour() {
        return hour;
    }

    public void setHour(double hour) {
        if (hour >= 0 && hour <= 168)
            throw new IllegalArgumentException("Hour must have in range 0 and 168");
        this.hour = hour;
    }

    public double getWage() {
        return wage;
    }

    public void setWage(double wage) {
        if (wage <= 0.0)
            throw new IllegalArgumentException("Wage must be greater than 0");
        this.wage = wage;
    }

    public double earnings() {
        return getHour()*getWage();
    }

    @Override
    public String toString() {
        return String.format("%n%s%n%s: %.2f%n%s: %.2f%n%s: %.2f",super.toString(),"Hour",getHour(),"Wage",getWage(),"Total earnings",earnings());
    }
}
