package collectionFramework.SetPractice.setpractice1;

public class Italian extends Dog {
    private String name;

    public Italian(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Dog) {
            if (this.getName() == ((Dog) obj).getName())
                {
                    return true;
                }
        } else
            {
                return false;
            }
        return false;
    }

    @Override
    public String getName() {
        return name;
    }
}
