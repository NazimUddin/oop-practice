package collectionFramework;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class MapTest {
    public static void main(String[] args) {
//        Map<String, String> husbandWifeMapping = new HashMap<>();
//        husbandWifeMapping.put("Jack", "Marie");
//        husbandWifeMapping.put("Chris", "Lisa");
//        husbandWifeMapping.put("Steve", "Jennifer");
//
//        String husband = "Chris";
//        String wife = husbandWifeMapping.remove(husband);
//        System.out.println(husbandWifeMapping);

        Map<String, String> map = new HashMap<>();
        map.put("key1", "value1");
        map.put("key2", "value2");
        map.put("key3", "value3");

        // Returns a Set view of the keys contained in this map
        Set<String> keys = map.keySet();

        // Returns a Collection view of the values contained in this map
        Collection<String> values = map.values();

        // Returns a Set view of the mappings contained in this map
        Set<Map.Entry<String, String>> entry = map.entrySet();

        // iterate map using java 8 forEach method
        map.forEach((k, v) -> {
            System.out.println(k);
            System.out.println(v);
        });

        for (Map.Entry<String, String> pair : entry) {
            System.out.println(pair.getKey());
            System.out.println(pair.getValue());
        }
    }
}
