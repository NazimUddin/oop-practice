package Generics.Q1;

public class GenericMethodTest {
    public static void main(String[] args) {
        // create arrays of Integer, Double and Character
        GenericMethodTest GMT = new GenericMethodTest();
        Integer[] intArray = {1, 2, 3, 4, 5};
        Double[] doubleArray = {1.1, 2.2, 3.3, 4.4, 5.5, 6.6, 7.7};
        Character[] charArray = {'H', 'E', 'L', 'L', 'O'};
        System.out.printf("Array integerArray contains:%n");
        GMT.printArray(intArray, 1, 4);
        System.out.printf("Array doubleArray contains:%n");
        GMT.printArray(doubleArray, 1.0, 7.5);
        System.out.printf("Array charArray contains:%n");
        GMT.printArray(charArray, 'A', 'L');

    }

    // generic method printArray
    public <T extends Comparable<T>> void printArray(T[] inputArray, T loweBound, T upperBound) {
        // display array elements
        for (T element : inputArray) {
            if (element.compareTo(upperBound) <= 0 && element.compareTo(loweBound) >= 0)
                System.out.printf("%s ", element);
        }
        System.out.println();
    }
}
