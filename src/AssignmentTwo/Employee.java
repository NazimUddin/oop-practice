package AssignmentTwo;

public class Employee {
    private String name;
    private byte rank;
    private String address;
    private String mobileNo;
    private BankAccount bankAccount;

    Employee(String name, byte rank, String address, String mobileNo, BankAccount bankAccount) {
        this.name = name;
        this.rank = rank;
        this.address = address;
        this.mobileNo = mobileNo;
        this.bankAccount = bankAccount;
    }

    @Override
    public String toString() {
        return String.format("%n%s: %s%n%s: %s%n%s: %s%n%s: %s%n%s",
                "Name", getName(), "Rank", getRank(), "Address", getAddress(), "Mobile No", getMobileNo(), getBankAccount().toString());
    }

    public String getName() {
        return name;
    }

    public byte getRank() {
        return rank;
    }

    public String getAddress() {
        return address;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public BankAccount getBankAccount() {
        return bankAccount;
    }

    public void setBankAccount(BankAccount bankAccount) {
        this.bankAccount = bankAccount;
    }
}
