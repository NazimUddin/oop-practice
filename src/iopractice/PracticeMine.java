package iopractice;

import java.io.*;

public class PracticeMine {
    public static void main(String[] args) {
        File file = new File("C:\\Users\\Asif\\IdeaProjects\\JavaOOPPractice\\src\\iopractice\\Simple.txt");
        PracticeMine practiceMine = new PracticeMine();
        practiceMine.readWriteBuffer(file);

    }
    public void readWriteBuffer(File file){
        try {
            BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(file, true));
            String l="this is a file for test.";
            bufferedWriter.write(l);
            bufferedWriter.newLine();
            bufferedWriter.flush();

            BufferedReader bufferedReader = new BufferedReader(new FileReader(file));
            String line;
            while ((line = bufferedReader.readLine())!=null){
                System.out.println(line);
            }
        }catch (IOException e){
            e.printStackTrace();
        }
    }
}
