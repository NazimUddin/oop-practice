package iopractice.accountPractice;

public class Account {
    private int account;
    private String firstName;
    private String lastName;
    private double balance;

    public Account(){
        this(0,"","",0.0);
    }

    public Account(int account, String firstName, String lastName, double balance) {
        this.account = account;
        this.firstName = firstName;
        this.lastName = lastName;
        this.balance = balance;
    }

    public String getFirstName() {
        return firstName;
    }

    public int getAccount() {
        return account;
    }

    public String getLastName() {
        return lastName;
    }

    public double getBalance() {
        return balance;
    }
}
