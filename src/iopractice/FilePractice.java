package iopractice;

import java.io.*;

public class FilePractice {
    public static void main(String[] args) {
        //List files and dirs
        FilePractice fileStream = new FilePractice();
//        fileStream.listFilesAndDirs();

        File file = new File("C:\\Users\\Asif\\IdeaProjects\\JavaOOPPractice\\src\\iopractice\\Simple.txt");
//        fileStream.createAndWrite(file);
//        fileStream.readFile(file);

//        fileStream.readWriteFileCharBased(file);
//        fileStream.readWriteBuffer(file);
    }

    public void listFilesAndDirs() {
        File file = new File("/home/jahangir/Documents");
        if (file.exists()) {
            if (file.isDirectory()) {
                File[] files = file.listFiles();
                for (File aFile : files) {
                    var name = aFile.getName();
                    var isDirectory = aFile.isDirectory();
                    String type = isDirectory ? "a directory" : "a file";
                    System.out.println(name + " is " + type);
                }
            }
        }
    }

    public void createAndWrite(File file) {
        OutputStream outputStream = null;
        try {
            if (!file.exists()) {
                file.createNewFile();
            }
            String text = "Hello world to all";
            outputStream = new FileOutputStream(file);
            outputStream.write(text.getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (outputStream != null) {
                try {
                    outputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void readFile(File file) {
        InputStream inputStream = null;
        try {
            inputStream = new FileInputStream(file);
            int content;
            while ((content = inputStream.read()) != -1) {
                System.out.print((char) content);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                inputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void readWriteFileCharBased(File file) {
        try {
            FileWriter fileWriter = new FileWriter(file, true);
            String textToWrite = "Hello World from file writer\n";
            fileWriter.write(textToWrite);
            fileWriter.close();

            FileReader reader = new FileReader(file);
            int content;
            while ((content = reader.read()) != -1 ) {
                System.out.print((char) content);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void readWriteBuffer(File file) {
        try {
            BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(file, true));
            bufferedWriter.write("From buffered writer");
            bufferedWriter.newLine();
            bufferedWriter.flush();

            BufferedReader bufferedReader = new BufferedReader(new FileReader(file));
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                System.out.println(line);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
