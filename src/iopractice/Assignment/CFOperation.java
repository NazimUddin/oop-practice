package iopractice.Assignment;

import java.io.*;
import java.util.*;
import java.util.concurrent.atomic.AtomicReference;

public class CFOperation {
    ArrayList<String> nameAndID = new ArrayList<>();
    ArrayList<Double> marksWithSubject = new ArrayList<>();
    Scanner input = new Scanner(System.in);

    File file = new File("C:\\Users\\Asif\\IdeaProjects\\JavaOOPPractice\\src\\iopractice\\Assignment\\Marks.txt");

    public static void main(String[] args) {

        CFOperation operation = new CFOperation();
        operation.takeInputOperation();
        try {
            operation.readFile();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void takeInputOperation() {
        System.out.println("How many entry do you want: ");
        int no = input.nextInt();

        int in = 0;
        while (in < no) {
            input.nextLine();
            System.out.println("Enter Name");
            nameAndID.add(input.nextLine());

            System.out.println("Enter ID");
            nameAndID.add(input.nextLine());

            System.out.println("Enter the marks of Bangla");
            marksWithSubject.add(input.nextDouble());
            System.out.println("Enter the marks of English");
            marksWithSubject.add(input.nextDouble());
            System.out.println("Enter the marks of Math");
            marksWithSubject.add(input.nextDouble());
            createFile();
            try {
                writeToFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
            nameAndID.clear();
            marksWithSubject.clear();

            in++;
        }

    }

    public void createFile() {
        if (!file.exists()) {
            try {
                file.createNewFile();
                BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(file, true));
                bufferedWriter.write("Name ID Bengali English Math Average");
                bufferedWriter.newLine();
                bufferedWriter.flush();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void writeToFile() throws IOException {
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(file, true));

        String line = getLine();
        bufferedWriter.write(line);
        bufferedWriter.newLine();
        bufferedWriter.flush();

    }

    public void readFile() throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new FileReader(file));
        String line;
        while ((line = bufferedReader.readLine()) != null) {
            System.out.println(line);
        }
        bufferedReader.close();
    }

    public String getLine() {
        String[] line = getLineFromMaps();
        String pureLine = "";
        for (int i = 0; i < 6; i++) {
            pureLine += line[i] + " ";
        }
        return pureLine;
    }

    public String[] getLineFromMaps() {

        double average = 0.0;
        String[] finalLine = new String[6];
        int i = 0;
        for (String element : nameAndID) {
            finalLine[i] = element + " ";
            i++;
        }
        for (double element : marksWithSubject) {
            average += element;
            finalLine[i] = String.valueOf(element);
            i++;
        }
        average = average / 3.0;
        finalLine[i] = String.valueOf(average);
        return finalLine;
    }

}
