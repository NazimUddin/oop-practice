package iopractice.IOAssigment;

public class Marks {
    private int Bangla;
    private int English;
    private int Math;
    private int Religion;

    public Marks(int bangla, int english, int math, int religion) {
        Bangla = bangla;
        English = english;
        Math = math;
        Religion = religion;
    }

    public int getBangla() {
        return Bangla;
    }

    public int getEnglish() {
        return English;
    }

    public int getMath() {
        return Math;
    }

    public int getReligion() {
        return Religion;
    }

}
