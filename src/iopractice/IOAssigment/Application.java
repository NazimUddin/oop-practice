package iopractice.IOAssigment;

import java.io.IOException;

public class Application {
    public static void main(String[] args) {
        FileHandler fileHandler = new FileHandler();
        fileHandler.generateInputFile();
        try {
            fileHandler.generateOutputFile();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
