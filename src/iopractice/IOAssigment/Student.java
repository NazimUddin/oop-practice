package iopractice.IOAssigment;

public class Student {
    private long id;
    private String name;
    private Marks marks;

    public Student(long id, String name, Marks marks) {
        this.id = id;
        this.name = name;
        this.marks = marks;
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Marks getMarks() {
        return marks;
    }


}
