package iopractice.IOAssigment;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;


public class GradManipulation {

    public ArrayList<String> createGrad() {
        File inputFile = new File("C:\\Users\\Asif\\IdeaProjects\\JavaOOPPractice\\src\\iopractice\\IOAssigment\\InputFile\\Marks.txt");
        ArrayList<String> finalAL = new ArrayList<>();
        String finalLine = "";
        FileHandler fileHandler = new FileHandler();
        ArrayList<String> data = null;
        try {
            data = fileHandler.readFile(inputFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
        int track = 0;
        for (String line : data) {
            if (track != 0) {
                String[] aStudent = line.split(",");
                finalLine += aStudent[0] + "," + aStudent[1] + ",";
                finalLine += obtainGradByStudent(aStudent);
                finalAL.add(finalLine);
                finalLine = "";
            }
            track++;
        }
        return finalAL;

    }

    public String obtainGradByStudent(String[] aStudent) {

        String line = "";
        double totalGrade = 0.0;

        for (int i = 2; i < 6; i++) {
            totalGrade += getPoint(Integer.parseInt(aStudent[i]));
            line += aStudent[i] + "," + getPoint(Double.parseDouble(aStudent[i])) + "," + getGrad(getPoint(Double.parseDouble(aStudent[i]))) + ",";
        }
        totalGrade /= 4.0;
        line += totalGrade + "," + getGrad(totalGrade);

        return line;

    }

    public double getPoint(double mark) {
        if (mark >= 80) {
            return  5.0;
        } else if (mark >= 70) {
            return 4.0;
        } else if (mark >= 60) {
            return 3.0;
        } else if (mark >= 50) {
            return 2.0;
        } else if (mark >= 40) {
            return 1.0;
        }else
            return 0.0;

    }

    public String getGrad(double point) {
        if (point >= 5.0) {
            return "A+";
        } else if (point >= 4.0) {
            return "A ";
        } else if (point >= 3.0) {
            return "A-";
        } else if (point >= 2.0) {
            return "B ";
        } else if (point >= 1.0) {
            return "C ";
        } else {
            return "F ";
        }
    }
}
