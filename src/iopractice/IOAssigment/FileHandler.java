package iopractice.IOAssigment;

import java.io.*;
import java.util.ArrayList;

public class FileHandler {

    public void generateInputFile() {
        String filePath = "C:\\Users\\Asif\\IdeaProjects\\JavaOOPPractice\\src\\iopractice\\IOAssigment\\InputFile\\Marks.txt";
        InputOperation inputOperation = new InputOperation();
        File inputFile = new File(filePath);
        createFile(inputFile);
        ArrayList<Student> allStudent = inputOperation.populateStudent();
        for (Student student : allStudent) {
            String line = "";
            line += takeInputFromUser(student);
            try {
                    writeInputFile(inputFile, line);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    public void createFile(File file) {
        if (!file.exists()) {
            try {
                file.createNewFile();
                BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(file, true));
                bufferedWriter.write("ID,Name,Bengali,English,Math,Religion");
                bufferedWriter.newLine();
                bufferedWriter.flush();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    public String takeInputFromUser(Student student) {
        String line = "";
        line += student.getId() + ",";
        line += student.getName() + ",";
        Marks marks = student.getMarks();
        line += marks.getBangla() + ",";
        line += marks.getEnglish() + ",";
        line += marks.getMath() + ",";
        line += marks.getReligion();
        return line;
    }

    public void writeInputFile(File file, String line) throws IOException {
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(file, true));

        bufferedWriter.write(line);
        bufferedWriter.newLine();
        bufferedWriter.flush();

    }

    public ArrayList<String> readFile(File file) throws IOException {
        ArrayList<String> data = new ArrayList<>();
        BufferedReader bufferedReader = new BufferedReader(new FileReader(file));
        String line;
        while ((line = bufferedReader.readLine()) != null) {
            data.add(line);
        }
        bufferedReader.close();
        return data;
    }

    public void generateOutputFile() throws IOException {
        GradManipulation GM = new GradManipulation();
        ArrayList<String> allStudentInfo = GM.createGrad();
        String[] aStudentInfo = new String[15];
        for (String aStudent : allStudentInfo) {
            aStudentInfo = aStudent.split(",");

            File file = new File("C:\\Users\\Asif\\IdeaProjects\\JavaOOPPractice\\src\\iopractice\\IOAssigment\\OutputFile\\" + aStudentInfo[0] + "-" + aStudentInfo[1] + ".txt");

            if (!file.exists()) {

                BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(file, true));

                bufferedWriter.write("Name: " + aStudentInfo[1] + " Roll: " + aStudentInfo[0]);
                bufferedWriter.newLine();
                bufferedWriter.write("----------------------------------------");
                bufferedWriter.newLine();
                bufferedWriter.write("Subject\t| Marks\t| Grade Point| Grade|");
                bufferedWriter.newLine();
                bufferedWriter.write("----------------------------------------");
                bufferedWriter.newLine();
                bufferedWriter.write("Bangla\t| " + aStudentInfo[2] + "\t| " + aStudentInfo[3] + "\t\t| " + aStudentInfo[4]+"\t|");
                bufferedWriter.newLine();
                bufferedWriter.write("----------------------------------------");
                bufferedWriter.newLine();
                bufferedWriter.write("English\t| " + aStudentInfo[5] + "\t| " + aStudentInfo[6] + "\t\t| " + aStudentInfo[7]+"\t|");
                bufferedWriter.newLine();
                bufferedWriter.write("----------------------------------------");
                bufferedWriter.newLine();
                bufferedWriter.write("Math\t| " + aStudentInfo[8] + "\t| " + aStudentInfo[9] + "\t\t| " + aStudentInfo[10]+"\t|");
                bufferedWriter.newLine();
                bufferedWriter.write("----------------------------------------");
                bufferedWriter.newLine();
                bufferedWriter.write("Religion| " + aStudentInfo[11] + "\t| " + aStudentInfo[12] + "\t\t| " + aStudentInfo[13]+"\t|");
                bufferedWriter.newLine();
                bufferedWriter.write("----------------------------------------");
                bufferedWriter.newLine();
                bufferedWriter.write("\t\t\tGPA\t| " + aStudentInfo[14] + "\t\t| " + aStudentInfo[15]+"\t|");
                bufferedWriter.newLine();
                bufferedWriter.write("----------------------------------------");

                bufferedWriter.flush();
            }
        }
    }

}
