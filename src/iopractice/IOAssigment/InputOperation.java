package iopractice.IOAssigment;

import java.util.ArrayList;


public class InputOperation {
    public ArrayList<Student> populateStudent() {
        ArrayList<Student> allStudent = new ArrayList<>();
        allStudent.add(new Student(1, "Asif", new Marks(60, 70, 80, 90)));
        allStudent.add(new Student(2, "Azam", new Marks(55, 66, 88, 99)));
        allStudent.add(new Student(3, "Baker", new Marks(90, 70, 88, 66)));
        allStudent.add(new Student(4, "Nishad", new Marks(88, 99, 52, 65)));
        allStudent.add(new Student(5, "Wahid", new Marks(63, 45, 54, 66)));
        allStudent.add(new Student(6, "Abir", new Marks(89, 63, 55, 69)));
        allStudent.add(new Student(7, "Fahim", new Marks(63, 66, 96, 88)));
        allStudent.add(new Student(8, "Mehedi", new Marks(50, 50, 62, 66)));
        allStudent.add(new Student(9, "Raj", new Marks(66, 96, 56, 39)));
        allStudent.add(new Student(10, "Mim", new Marks(99, 87, 56, 63)));
        return allStudent;
    }


}
