package Assaignment.Q4;

public class Employee {
    private double baseSalary;
    private double hourlyRate;
    private double extraRate;

    Employee(double baseSalary, double hourlyRate, double extraRate) {
        if (baseSalary <= 0) {
            throw new IllegalArgumentException("Base Salary cannot be less then or equal 0");
        }
        if (hourlyRate <= 0) {
            throw new IllegalArgumentException("Hourly Rate cannot be less then or equal 0");
        }
        if (extraRate <= 0) {
            throw new IllegalArgumentException("Extra Rate can not be less then or equal 0");
        }
        this.baseSalary = baseSalary;
        this.hourlyRate = hourlyRate;
        this.extraRate = extraRate;
    }

    public double getBaseSalary() {
        return baseSalary;
    }


    public double getHourlyRate() {
        return hourlyRate;
    }


    public double getExtraRate() {
        return extraRate;
    }


    public double calculateTotalSalary() {
        return getBaseSalary() + (getHourlyRate() * getExtraRate());
    }
}
