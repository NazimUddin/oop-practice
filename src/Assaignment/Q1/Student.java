package Assaignment.Q1;

public class Student {
    private String Name;

    Student() {
        this("Unknown");
    }

    Student(String Name) {
        this.Name = Name;
    }

    public String getName() {
        return Name;
    }

    @Override
    public String toString() {
        return String.format("%s : %s%n", "Name is", getName());
    }
}
