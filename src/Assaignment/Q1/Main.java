package Assaignment.Q1;

public class Main {

    public static void main(String[] args) {
        Student[] student = new Student[4];

        student[0] = new Student("Asif");
        student[1] = new Student();
        student[2] = new Student("Karim Ullah");
        student[3] = new Student();

        for (Student currentStudent : student) {
            System.out.printf("%s", currentStudent.toString());
        }
    }

}
