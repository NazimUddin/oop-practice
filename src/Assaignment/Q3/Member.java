package Assaignment.Q3;

public class Member {
    private String name;
    private int age;
    private String phoneNo;
    private String address;
    private double salary;

    Member(String name, int age, String phoneNo, String address, double salary) {
        this.name = name;
        this.age = age;
        this.phoneNo = phoneNo;
        this.address = address;
        this.salary = salary;
    }

    public void printSalary() {
        System.out.printf("%s : %s%n", "Salary is ", salary);
    }
}
