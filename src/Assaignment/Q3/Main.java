package Assaignment.Q3;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Member member;
        System.out.println("Press 1 for Employee and 2 for Manager");
        int no = scanner.nextInt();
        member = getMember(no);
        member.printSalary();

    }

    public static Member getMember(int no) {
        switch (no) {
            case 1:
                return new Employee("Khalil", 24, "000-1569-25", "feni sadar", 40000);
            case 2:
                return new Manager("Baki", 24, "000-1569-25", "feni sadar", 500000);
        }
        return null;
    }
}
